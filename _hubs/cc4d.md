---
title: Community Creativity For Development (CC4D)
subtitle: Community Creativity for Development (CC4D) is a Youth led community-based organization formed in August 2019 formally and was founded by South Sudanese Refugees professionals (Youths) who saw a gap in repair, reuse and maintenance of Electronics. CC4D already has a presence on the ground, implementing digital literacy training programs and mobile repairs and maintenance of electronics with its main aim “To digitally connect Communities while protecting the environment from carbon emission and landfill using the available resources for self-reliance and sustainable development.”
image: assets/img/hubs/06-full.jpg
alt: Community Creativity For Development (CC4D)
layout: hub

caption:
  title: Community Creativity For Development (CC4D)
  subtitle: Rhino Camp, Uganda
  thumbnail: assets/img/hubs/06-thumbnail.jpg
---

{:.list-inline .hub-summary}
| Location: | Eden II, Rhino Camp, Uganda |
| --------- | ------------------ |
| Mission:  | CC4D-Uganda strives to connect communities while protecting the environment from global warming using the available resources for self-reliance and sustainable development |
| Vision:  | “A transformed and committed society fully in control of their social economic wellbeing using the available resources to reduce poverty, building peaceful and prosperous communities.” |

{:.logo}
![CC4D]({{ site.baseurl }}/assets/img/hubs/06-logo.png)

{:.text-center}
<a href="https://www.facebook.com/CC4D.Uganda/" class="btn btn-primary visit-website" target="_blank"><i class="fab fa-facebook-f"></i></a>

CC4D-UGANDA aspires to implement sustainable community intervention initiatives targeted at positively transforming both the refugees and the communities that host them, with an emphasis on safe use of information and communication technology (ICT) while protecting the environment from global warming and improve the lives of both the refugees and host community in Uganda. Open access to information and Information broadcasting is an important aspect of our strategic plan to impact communities in which we operate. Improving the health and well-being of community residents is important to us, for example, through:

- Organizing repair café events
- Advocacy on repair and reuse
- Access to information and Hardware
- Providing digital literacy skills
- Awareness raising on E-Waste management
- Collection and recycling of E-waste
- Promotion of peaceful coexistence

![CC4D]({{ site.baseurl }}/assets/img/hubs/06-content-1.jpg)

![CC4D]({{ site.baseurl }}/assets/img/hubs/06-content-2.jpg)

#### CORE VALUES

1. Determination and commitment
2. Faithful
3. Inclusive community
4. Human dignity
5. Transparency and Accountability
6. Teamwork and Cohesive team building
7. Integrity
8. Creativity
9. Zero tolerance to corruption
10. Result oriented
11. Environmental justices and sustainable community development

#### THEMATIC AREAS OF OPERATION

1. Environmental protection
2. Youth empowerment on digital literacy and Livelihood Support
3. Peace Building

![CC4D]({{ site.baseurl }}/assets/img/hubs/06-content-3.jpg)

![CC4D]({{ site.baseurl }}/assets/img/hubs/06-content-4.jpg)

![CC4D]({{ site.baseurl }}/assets/img/hubs/06-content-5.jpg)

![CC4D]({{ site.baseurl }}/assets/img/hubs/06-content-6.jpg)

