---
layout: page
title: Media
---

# MEDIA

Welcome to the #ASKnet Media Hub, your gateway to thought-provoking content, insightful discussions, and inspiring stories. Our Media Hub is a platform that offers a diverse range of engaging content, including podcasts, blogs, newsletters, and more. Explore the world of knowledge, culture, and innovation through our media initiatives.

## Podcasts:

Tune in to our captivating podcasts that bring together experts, activists, and change-makers from diverse fields. Gain valuable insights into pressing issues, community-driven solutions, and stories of resilience and empowerment. Our podcasts are designed to spark meaningful conversations and provide a deeper understanding of the challenges and opportunities in South Sudan. Whether you are looking to expand your knowledge or seek inspiration, our podcasts are sure to leave you enlightened.

### Amplified Gender Voices

Hello! And welcome to the Amplified Gender Voices! A podcast platform that entails amplifying the voices of women and girls in the communities of South Sudan, this podcast shall cover stories, interviews, and conversations by South Sudanese and stakeholders as they discuss, converse, and push for policies and solutions to the derailing problems that face women and girls as far as getting involved in STEM (Science Technolgy Engineering and Mathematics) is concerned

This is a GoGirls ICT Podcast!

[https://soundcloud.com/agvpodcast](https://soundcloud.com/agvpodcast)

![woman speaking into microphone]({{ site.baseurl }}/assets/img/media-page/agvpodcast.jpeg){:style="max-width: 300px"}

### Refugee Youth Podcast

The Refugee Youth Podcast is a Youth Empowerment podcast,  platform that shares vital information on refugee issues, providing a voice to those directly affected. This program is driven by the passion and dedication of refugee youth who curate and produce engaging content, ensuring that accurate and relevant information reaches their communities. 🗣💡

We believe that access to information is crucial for empowering individuals and creating positive change. Through the Refugee Youth Podcast, we are bridging the gap and amplifying the voices of refugees, allowing them to share their experiences, insights, and solutions. 🎧✨

We invite you to tune in, listen, and support the Refugee Youth Podcast as we strive to foster awareness, understanding, and solidarity. Together, we can break barriers, empower refugees, and create a more inclusive society. 🤝💙

[https://soundcloud.com/refugee-youth-podcast](https://soundcloud.com/refugee-youth-podcast)

![refugee youth podcast logo]({{ site.baseurl }}/assets/img/media-page/rypodcast.png){:style="max-width: 300px"}

### ASK podcast

A Podcast that features stories from innovators, hubs and innovation spaces on how they are solving local community challenges in post-conflict or conflict areas.

[https://podcasters.spotify.com/pod/show/ask-podcast6/](https://podcasters.spotify.com/pod/show/ask-podcast6/)

## Blogs:

Delve into the minds of thought leaders, creatives, and passionate individuals who share their perspectives through our thought-provoking blogs. Our blog section covers a wide array of topics, ranging from culture and society to technology and innovation. Experience the power of storytelling as we bring you compelling narratives that shed light on South Sudan’s journey towards positive change. Join us in exploring the unique experiences and innovative ideas shaping our communities.

## Newsletters:

Stay updated with the latest happenings, achievements, and initiatives within the #ASKnet community through our informative newsletters. Our newsletters are carefully curated to provide you with in-depth insights, event highlights, and success stories of our consortium members. Be the first to know about upcoming events, workshops, and networking opportunities as we create a culture of information-sharing and collaboration. Subscribe to our newsletters and be part of our ever-growing community.

## Resource Library:

Access a wealth of knowledge and information through our comprehensive resource library. Discover educational materials, research papers, and reports on topics ranging from community development to sustainable practices. Our resource library is a valuable repository that empowers you with the tools needed to drive positive change in your community. Explore, learn, and apply this knowledge to make a lasting impact.

Join us in this enriching journey of exploration, learning, and inspiration through the #ASKnet Media Hub. Engage with our podcasts, immerse yourself in our blogs, and stay informed with our newsletters. Together, we can build a media ecosystem that fosters open communication, embraces diversity, and empowers individuals to shape a brighter future for South Sudan.

Empower yourself with knowledge. Explore the #ASKnet Media Hub today!

Let’s work together to create a transformative and sustainable media ecosystem in South Sudan. Join #ASKnet today and make a difference!